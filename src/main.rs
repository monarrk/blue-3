use std::env;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::process::Command;

use serenity::model::{channel::Message, channel::ReactionType, gateway::Ready};
use serenity::prelude::*;
use serenity::utils::Colour;
use serenity::Client;

/// Send a message to $m.channel_id with the content $x
macro_rules! say {
    ( $m:ident, $c:ident => $x:expr ) => {{
        println!("[blue-3] Sending message \"{}\"", $x);
        if let Err(e) = $m.channel_id.say(&$c.http, $x) {
            eprintln!("Error sending message: {:#?}", e);
        }
    }};
}

/// React to $m with $x (unicode only)
macro_rules! react {
    ( $m:ident, $c:ident => $x:expr ) => {{
        println!("[blue-3] Reacting with \"{}\"", $x);
        if let Err(e) = $m.react(&$c.http, ReactionType::Unicode($x)) {
            eprintln!("Error reacting to message: {:#?}", e);
        }
    }};
}

/// Add a clown to the list
macro_rules! add_clown {
    ( $x:expr ) => {{
        println!("[blue-3] Adding clown \"{}\"", $x);
        unsafe {
            CLOWNS.push($x);
        }
    }};
}

// yikes
static mut CLOWNS: Vec<u64> = Vec::new();

struct Handler;

impl EventHandler for Handler {
    fn message(&self, ctx: Context, msg: Message) {
        // unsafe here because CLOWNS is static mut
        unsafe {
            if CLOWNS.contains(msg.author.id.as_u64()) {
                react!(msg, ctx => String::from("🤡"));
            }
        }

        // Match the message content
        match msg.content.as_str() {
            "b3>info" => {
                // get output of uname -a
                let uname = match Command::new("uname").arg("-a").output() {
                    Ok(c) => match String::from_utf8(c.stdout) {
                        Ok(s) => s,
                        Err(e) => {
                            eprintln!("Error converting output to utf8: {}", e);
                            return;
                        }
                    },
                    Err(e) => {
                        eprintln!("Error running uname -a: {}", e);
                        return;
                    }
                };

                let sysinfo = format!("{} : {}", env::consts::OS, uname);
                msg.channel_id.send_message(&ctx, |m| {
                    m.embed(|e| {
                        e.color(Colour::RED);
                        e.title("blue-3");
                        e.description("blue-3 clowns clowns.");
                        e.field("version", env!("CARGO_PKG_VERSION"), false);
                        e.field("language", "rust", false);
                        e.field("sysinfo", sysinfo, false);
                        e
                    });
                    m
                });
            }
            "b3>hello" => say!(msg, ctx => "Hello from rust!"),
            "b3>clown" => react!(msg, ctx => String::from("🤡")),
            "b3>get" => {
                let mut s = String::new();

                // Join all the ids into `s`
                unsafe {
                    for id in CLOWNS.iter() {
                        s.push_str(&format!("{}\n", id));
                        println!("[blue-3] Added clown {}", id);
                    }
                }

                say!(msg, ctx => s);
            }

            _ => (),
        };
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Getting token from E:DISCORD_TOKEN...");
    let token = env::var("DISCORD_TOKEN")?;

    // Prepare the clown conf file
    println!("[blue-3] Reading config...");
    let conf = File::open("blue-3.conf")?;
    let reader = BufReader::new(conf);

    for line in reader.lines() {
        let line = line?.parse::<u64>()?;
        // Add the clown the the list
        add_clown!(line);
    }

    println!("[blue-3] Creating client");
    let mut client = Client::new(&token, Handler)?;

    println!("[blue-3] Starting client");
    client.start().expect("failed to start client");

    Ok(())
}
